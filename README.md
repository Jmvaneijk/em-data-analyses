# Electron Microscopy Pixel Identification #

## Introduction ## 

Normally, images created by *Electron Microscopy*(EM) are colored by hand, which is a time-consuming processes. 
This process however, can also be autonomized, making it faster and more efficient by removing the human factor.

Using Keras-Tensorflow combined with the image manipulation abilities of Skimage and array power of Numpy, 
large images can be chopped into pieces and classified using a neural network. These resulting images and models
can then be saved to be used at a later time.

Within this repo you can find the code and notebook responsible for this whole classification process, alongside
pre-trained models and some training data for your own model.


## Installation -- Linux ##

Before you can start with the classifications, some preparatory steps need to be made.


### Creating the virtual environment ###

Before installing the package into the virtual environment, a virtual environment should be created. 
this can be done with the virtualenv package and can be installed as follows.

Open a terminal in the directory of the project and install the virtualenv package, if it is not installed yet.

```commandline
# Can also be done with pip, but 3 specifies python3.
pip3 install virtualenv
```

With virtualenv installed, the environment can be created like this.

```commandline
# I'm calling the environment 'EM_venv', but you can name it whatever you want.
virtualenv -p /usr/bin/python3 EM_venv
```

After the EM_venv is created you can load it and install the packages within.

```commandline
source EM_venv/bin/activate
```

The virtual environment can simply be disabled by typing.

```commandline
deactivate
```


### Installing the packages ###

Deep learning is an important part of this project, and without it, images cannot be classified. So the first packages
to install are [Keras](https://keras.io/) and [Tensorflow](https://www.tensorflow.org/), 
which are both specialized in machine- and deep-learning.

```commandline
pip3 install keras
pip3 install tensorflow
```

Because the Keras model takes arrays as input, [Numpy](https://numpy.org/) is required to transfer the read image
into an array, and to transfer the instances into arrays.

```commandline
pip3 install numpy
```

To actually read the image, [Opencv-Python](https://opencv.org/) was used.
Opencv-Python can read the images as RGB or Greyscale and reshape / resize them on demand.

```commandline
pip3 install opencv-python
```

Since the EM-images can be quite large, they are divided into chunks which are processed separately, or potentially in
parallel. This chunking step is done using Skimage,
which is part of the [Scikit-image](https://scikit-image.org/) package.

```commandline
pip3 install scikit-image
```

In order to render to classified image, or to show the images in the notebook,
[Matplotlib](https://matplotlib.org/) is used.

```commandline
pip3 install matplotlib
```

When the classification process is finished, the user has the option to validate their classified image, by 
providing a correctly colored version of the image. This validation step calculates a confusion matrix and allows
for the calculation of relevant statistics like the accuracy, precision and recall,
using the package [Scikit-learn](https://scikit-learn.org/).

```commandline
pip3 install scikit-learn
```

With all the packages have been installed, you can now close the virtual environment,
since it no longer needs to be activated.


### Installing Jupyter Notebook ###

After the virtual environment has been prepared, Jupyter notebook needs to be installed 
and given the virtual environment to run the created pipeline.

Just like the other python packages, jupyter is also installed using pip. However, in this case it is recommended to
install jupyter outside the venv, for easier access. 

```commandline
# Jupyter Notebook Installation
pip3 install notebook
```

The next step is to add the virtual environment as a kernel option to jupyter notebook.

```commandline
# Ipykernal Installation
pip3 install ipykernel

# Setting the virtual environment as a kernel option. (If the environment has a different name, use that one)
python3 -m ipykernal intall --user --name=EM_venv
```

With the notebook prepared and the kernel set, you can simply start jupyter notebook with the following command.

```commandline
jupyter notebook
```


## Getting Started ##

All the preparations and installations are now finished. This means that it is time to start the notebook which will
serve as the pipeline for the process.


### Starting the notebook ###

If you are in the root of the directory, start jupyter notebook
and navigate to *Reports > Notebooks > EM_Classification_Pipeline.ipynb*.


### Setting the kernel ###

After starting the notebook, we have to set the kernel to the created virtual environment. 
On the top bar click on *Kernel > Change Kernel > EM_Venv* and this should set the kernel to the created environment.


### Setting the input params ###

Before running the code, the parameters need to be set. Most of these are already on their default values,
like the learning rate, but if the model performs poorly these can also be adjusted. The empty parameters however should
be filled before running the code.

If you have no training images, color layers or other important parameters, there are some dummy datasets which can be 
found within the data folder on the repository.

When you would like to revert to the basic settings, use the following code as reference.

```python
    # File Related Information
    #   Training data using a normal EM-image for the pixel information
    train_image = ""
    
    #   A Color layer for the label information
    color_layer = ""

    #   Test image to classify
    test_image = ""

    #   Output file for the classified result
    outfile = ""

    
    # Image Manipulation Settings.
    #   Scale space levels (e.g. [2, 4], for additional 2 and 4 factor zooms)
    scale_list = [2, 4]

    #   Neighbor range (Amount of neighboring pixels to use)
    neighbor_range = 3

    #   Chunk size (Images are cut into smaller chunks, and processed one by one, to save memory)
    #   Recommended to be atleast 100+
    chunk_size = 200

    #   If the images should be resized before chunking, provide the new size, else use None
    resize = 1000

    
    # Model Related Information
    #   Epochs (Amount of training iterations)
    epochs = 20

    #   Batch Size (Subset size of the training data within each epoch)
    batch_size = 100

    #   Learning rate using Adam
    learning_rate = 1e-3

    #   Classification threshold (The model needs this amount of certainty to label a pixel)
    threshold = 0.95

    #   Save Model Location (If the model should not be saved, use None)
    model_checkpoint = None
    
    # Pre-loaded model Location (If an older model should be loaded) If the model should be build, use None
    saved_model = None
```

### Running the notebook ###

To start the process, navigate to the top bar and click *Kernel > Restart & Run All*. This will run all the code
in the notebook and show the classified image at the end

### Validation ###

If you want to validate the results from the pipeline, a tutorial can be found in the Validation notebook.
Unlike the classification pipeline, this notebook can not simply be used after setting the parameters, due to
the validation being specific to classes in the image. This notebook however, can function as a guideline for your
own potential validations.

## Contact ##
If you have question, suggestions or issues, feel free to contact us at: 
 - g.h.bakker@st.hanze.nl
 - ja.m.van.eijk@st.hanze.nl

And we will try to get back to you as soon as possible!
