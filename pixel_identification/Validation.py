"""
This module contains 2 classes related to result validation

The first class is the ConfusionMatrix class. Using Skimage-Learn the predicted and actual values will be
compared with each other and summarized in a confusion matrix. This confusion matrix will be a dictionary
containing dictionaries for each of the labels with the
True Positive, True Negative, False Positive and False Negative values in them for easy access

The other class is the Validator class. This class takes a single label dictionary from the confusion matrix
and allows for the calculation of various statistics
"""


# IMPORTS
#   PYTHON MODULES
from sklearn.metrics import multilabel_confusion_matrix
import sys

#   CUSTOM MODULES
from pixel_identification.functions import get_key
from pixel_identification.image_manipulator import image_to_array


# META INFORMATION
__author__ = "Gijs Bakker & Jamie van Eijk"
__version = "1.0.0"


# CLASSES
class ConfusionMatrix:
    """
    By providing the classified image, a correct color layer of the classified image and the color dictionary
    the pixels can be compared and turned into a confusion matrix describing the True Positive, True Negative,
    False Positive and False Negative per label in a nested dictionary format for easy index access.
    """

    def __init__(self, classified_image, color_layer, color_dict, resize):
        """
        Setting the init parameters
        :parameter classified_image: The classified image file / array
        :parameter color_layer: Corresponding color layer of the classified image as file / array
        :parameter color_dict: The dictionary with the available labels
        :parameter resize: Resize value making sure the classified image and color layer are the same size
        """
        self.color_dict = color_dict
        self.rgb = True

        # If the images are string, they are converted to RGB arrays
        self.classified_image = self.__image_as_array(classified_image, resize)
        self.color_layer = self.__image_as_array(color_layer, resize)

        # Creating the confusion matrix
        self.confusion_matrix = self.__create_confusion_matrix()

    def __image_as_array(self, image, resize):
        """
        Checks if the provided image is a string or array. When the image is already an array, just return it,
        else convert the image to a Numpy array and return it
        """
        if isinstance(image, str):
            image = image_to_array(image, resize, self.rgb)

        return image

    def __create_confusion_matrix(self):
        """
        Compares the classified image with the color layer, per pixel,
        to find the amount of True Positives, False Positive, True Negatives and False negatives per class
        and save these in a matrix
        """
        # Empty containers for predicted and validation labels
        y_true = []
        y_pred = []

        # Labels container
        labels = list(self.color_dict.keys())
        labels.append(len(self.color_dict))

        # Checking each pixel and storing the labels as lists
        for validation_pixels, classified_pixels in zip(self.color_layer, self.classified_image):
            for validation_pixel, classified_pixel in zip(validation_pixels, classified_pixels):
                self.__add_label(y_true, validation_pixel)
                self.__add_label(y_pred, classified_pixel)

        # Using Scikit-Learn to create a 3D confusion matrix
        conf_matrix = multilabel_confusion_matrix(y_true, y_pred, labels=labels)

        # Convert the 3D confusion matrix to a nested dictionary structure
        conf_matrix_dict = {label: {"TN": matrix[0][0], "FP": matrix[0][1], "FN": matrix[1][0], "TP": matrix[1][1]}
                            for matrix, label in zip(conf_matrix, labels)}

        return conf_matrix_dict

    def __add_label(self, labels_list, pixel):
        """Using the color dict information, the labels are collected and assigned to the list"""
        # Changing the pixel from numpy array to list
        pixel = list(pixel)

        # Values Container
        values = list(self.color_dict.values())

        # If the pixel is classified, save the label in the labels list
        if pixel in values:
            labels_list.append(get_key(self.color_dict, pixel))

        # If the pixel is not classified, save it as 'unclassified' label
        else:
            labels_list.append(len(self.color_dict))

    def get_confusion_matrix(self):
        """Returns the confusion matrix"""
        return self.confusion_matrix


class Validator:
    """
    The Validator takes a single label dictionary from the confusion matrix and allows for the calculation
    of various statistics like the accuracy, recall or precision
    """
    def __init__(self, confusion_matrix):
        """
        Setting the init parameters
        :parameter confusion_matrix: The confusion matrix belonging to a label
        """
        self.confusion_matrix = confusion_matrix

    def get_precision(self):
        """Returns the precision for the requested label"""
        return self.confusion_matrix["TP"] / (self.confusion_matrix["TP"] + self.confusion_matrix["FP"])

    def get_recall(self):
        """Returns the recall for the requested label"""
        return self.confusion_matrix["TP"] / (self.confusion_matrix["TP"] + self.confusion_matrix["FN"])

    def get_accuracy(self):
        """Returns the accuracy for the requested label"""
        return (self.confusion_matrix["TP"] + self.confusion_matrix["TN"]) / sum(self.confusion_matrix.values())


# MAIN
def main():
    """Main function for potential testing"""
    return 0


if __name__ == "__main__":
    sys.exit(main())
