"""
This module contains 2 classes related to image manipulation

The image deconstructor class allows the user to split a (large) image into smaller chunks
After the user is finished with these chunks, or some other chunks in general, the image constructor class
can be used to turn the chunks into one large array,
and possibly render it to a .png file, if the output name has been provided

Besides these 2 classes, the module also contains a commonly used function for creating image arrays
Because it would be useless to create a De- or Reconstructor object every time an array needs to be created somewhere,
this function will serve as an independent function to be called on
"""


# IMPORTS
import cv2
from matplotlib import pyplot as plt
import numpy as np
import skimage
import sys


# META INFORMATION
__author__ = "Gijs Bakker & Jamie van Eijk"
__version = "1.0.0"


# BASIC FUNCTIONS
def image_to_array(image_file, resize=None, rgb=False):
    """Commonly used function to convert an image file to an array"""
    # Converting the image to a 2D / 3D array, depending on the image being RGB or Greyscale
    if rgb:
        image_array = cv2.imread(image_file, cv2.IMREAD_COLOR)
        image_array = cv2.cvtColor(image_array, cv2.COLOR_BGR2RGB)
    else:
        image_array = cv2.imread(image_file, cv2.IMREAD_GRAYSCALE)

    # Turn the image array into a numpy array
    image_array = np.array(image_array)

    # Resizing the image if requested
    if resize is not None:
        image_array = cv2.resize(image_array, (resize, resize), interpolation=cv2.INTER_NEAREST)

    # Returning the array for further use
    return image_array


# CLASSES
class ImageDeconstructor:
    """
    Using the image deconstruction class, a large image can be resized into smaller chunks which could then
    be parsed one by one, or spread out using multiprocessing
    """

    def __init__(self, image, chunk_size=200, size=None, rgb=False, scaling_factors=None):
        """
        Setting the init parameters
        :parameter image: Image file to be loaded in and chunked
        :parameter chunk_size: Size of the chunks that the image should be divided into
        :parameter size: Size that the image should be resized to, None if resizing is not needed
        :parameter rgb: True or False depending on the image being Greyscale or RGB
        :parameter scaling_factors: List of scaling factors for scale space, None if scale space is not needed
        """
        self.image = image
        self.chunk_size = chunk_size
        self.size = size
        self.rgb = rgb

        # Creating containers for the chunks and the downscaled chunks (scale space)
        self.chunks = np.array([])
        self.downscaled = []

        # Turning the image into an array
        self.image_array = image_to_array(image, size, rgb)

        # Round the image size
        self.__round_image_size()

        # Create chunk objects
        self.chunks = self.__create_chunks(self.image_array, self.chunk_size)

        # Saving the original shape of the chunks
        self.original_shape = self.chunks.shape

        # Creating downscaled chunks if scaling factors have been provided
        if scaling_factors:
            self.__set_downscaled(scaling_factors)

    def __repr__(self):
        """Message when printing object"""
        return f"Deconstructor for processing {self.image} into chunks of {self.chunk_size}px x {self.chunk_size}px"

    def __round_image_size(self):
        """
        Using floor division, image sizes that are not dividable by the chunk size, are resized so they are
        (e.g. 5763px x 3451px -> 5700px x 3400px, for 100px x 100px chunks)
        """
        # Using floor division, the rounded height and width are calculated
        image_shape = self.image_array.shape
        rounded_height = image_shape[0] - (image_shape[0] % self.chunk_size)
        rounded_width = image_shape[1] - (image_shape[1] % self.chunk_size)

        # Resizing the image to the rounded height and width
        self.image_array = cv2.resize(self.image_array, (rounded_width, rounded_height),
                                      interpolation=cv2.INTER_NEAREST)

    def __create_chunks(self, image_array, chunk_size):
        """Using Skimage, the image is cut in smaller chunks with can be iterated over"""
        # If the image is RGB, it will be cut into 3D chunks
        if self.rgb:
            return skimage.util.shape.view_as_blocks(image_array,
                                                     block_shape=(chunk_size, chunk_size, 3))

        # Else, if the image is greyscale, it will be cut in 2D chunks
        else:
            return skimage.util.shape.view_as_blocks(image_array,
                                                     block_shape=(chunk_size, chunk_size))

    def __set_downscaled(self, scaling_factors):
        """This function scales down the image for each given scaling factor when using scale space"""
        for scaling in scaling_factors:
            resized = cv2.resize(self.image_array, (int(len(self.image_array[0]) / scaling),
                                                    int(len(self.image_array) / scaling)))

            self.downscaled.append([scaling, self.__create_chunks(resized, int(self.chunk_size/scaling))])

    def get_original_shape(self):
        """Returns the original shape of the chunk structure"""
        return self.original_shape

    def get_chunks(self):
        """Returns the chunks"""
        return self.chunks

    def get_downscaled_chunks(self):
        """Returns the list with downscaled chunks, if scale space was applied"""
        if self.downscaled:
            return self.downscaled
        else:
            print("Unable to return downscaled chunks, because no scaling factors were provided")


class ImageConstructor:
    """
    The ImageConstructor can refold the chunked structures into a complete image array,
    after which the array can be rendered and saved to an output file, if provided
    """

    def __init__(self, chunks, outfile_name=None):
        """
        Setting the init parameters
        :parameter chunks: A Numpy array containing the chunks in a 1D structure
        :parameter outfile_name: Output file for the rendered image, None if it should not be saved
        """
        self.chunks = chunks
        self.outfile_name = outfile_name

        # Complete image array container
        self.image_array = np.array([])

        # Create the array from the chunks
        self.__construct_array()

    def __repr__(self):
        """Message when printing object"""
        return f"Array with shape: {self.image_array.shape}, constructed from chunks with shape: {self.chunks.shape}"

    def __construct_array(self):
        """Using transpose and reshape, the chunks are reconstructed into a complete array"""
        if self.chunks.shape[-1] == 3:
            self.image_array = self.chunks.transpose(0, 3, 1, 4, 5, 2)
            self.image_array = self.image_array.reshape(-1, self.chunks.shape[1] * self.chunks.shape[3], 3)
        else:
            self.image_array = self.chunks.transpose(0, 2, 1, 3)
            self.image_array = self.image_array.reshape(-1, self.chunks.shape[1] * self.chunks.shape[3])

    def get_constructed_array(self):
        """Returns the complete array"""
        return self.image_array

    def render_image(self):
        """Renders the created array as an png file"""
        plt.axis("off")
        plt.imshow(self.image_array)
        if self.outfile_name is not None:
            print(f"Output file provided, saving to: {self.outfile_name}")
            plt.savefig(self.outfile_name, bbox_inches="tight")
        else:
            print("No output file has been provided, just showing the rendered image")


# MAIN
def main():
    """Main function for potential testing"""
    return 0


if __name__ == "__main__":
    sys.exit(main())
