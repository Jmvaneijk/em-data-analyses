"""
This module contains the pixel class.

When parsing an image, each pixel of interest can be stored as a pixel object
The pixel objects will contain the coordinate of itself in the image, their neighbor range / neighbors and
the label if they should serve as training instances
"""


# IMPORTS
import sys
import math


# META INFORMATION
__author__ = "Gijs Bakker & Jamie van Eijk"
__version = "1.0.0"


# CLASSES
class Pixel:
    """
    When parsing the image, each pixel of interest can be stored as a pixel object.
    The pixel objects will contain the coordinate of itself in the image, their neighbor range / neighbors and
    the label if they should serve as training instances"""
    def __init__(self, coord, neighbor_range, label=None):
        """
        Setting the init parameters
        :parameter coord: Tuple with the coordinates of the pixel in the image
        :parameter neighbor_range: The range of the neighbors that will serve as attributes
        :parameter label: The label of the pixel, None if it should be classified
        """
        self.coord = coord
        self.neighbor_range = neighbor_range
        self.label = label

        # Container for the neighbors, including the pixel itself as the first item
        self.neighbors = []

    def __repr__(self):
        """Message when printing object"""
        return f'Pixel found at {self.coord}, labeled as {self.label}\n'

    def set_close_pixels(self, neighbor_chunks, scaling=None):
        """
        For each marked pixel, the surrounding pixels are collected and saved as attributes
        :parameter neighbor_chunks: The main chunk being processed, along with the 8 surrounding chunks
        :parameter scaling: The scaling factors if scale space is being used, otherwise None
        """

        # Setting some important variables
        img_array = neighbor_chunks[4]  # Middle item in the chunks list is the chunk currently being processed
        height = len(img_array)
        width = len(img_array[0])
        coord = self.coord

        # Scaling if requested
        if scaling:
            coordx = math.ceil(coord[0]/scaling)
            coordy = math.ceil(coord[1]/scaling)
            coord = (coordx, coordy)

        # Iterating over each pixel
        for coord_y in range(coord[1] - self.neighbor_range,
                             coord[1] + self.neighbor_range):
            for coord_x in range(coord[0] - self.neighbor_range,
                                 coord[0] + self.neighbor_range):

                # If the neighbor is within the same chunk, append the normalized value to the list
                if 0 <= coord_x < width and 0 <= coord_y < height:
                    self.neighbors.append(img_array[coord_y, coord_x] / 255)

                # Else check the surrounding chunks for neighbors
                else:

                    # Top sided chunks
                    if 0 > coord_y:

                        # Top left corner
                        if 0 > coord_x and neighbor_chunks[0] is not None:
                            self.neighbors.append(neighbor_chunks[0][coord_y, coord_x] / 255)

                        # Top middle
                        elif 0 <= coord_x < width and neighbor_chunks[1] is not None:
                            self.neighbors.append(neighbor_chunks[1][coord_y, coord_x] / 255)

                        # Top right corner
                        elif coord_x >= width and neighbor_chunks[2] is not None:
                            self.neighbors.append(neighbor_chunks[2][coord_y, coord_x - width] / 255)

                        # Edge case with no chunk for that neighbor, so adding 0 indicating black is added
                        else:
                            self.neighbors.append(0)

                    # Bottom sided chunks
                    elif coord_y >= height:

                        # Bottom left corner
                        if 0 > coord_x and neighbor_chunks[6] is not None:
                            self.neighbors.append(neighbor_chunks[6][coord_y - height, coord_x] / 255)

                        # Bottom middle
                        elif 0 <= coord_x < width and neighbor_chunks[7] is not None:
                            self.neighbors.append(neighbor_chunks[7][coord_y - height, coord_x] / 255)

                        # Bottom right corner
                        elif coord_x >= width and neighbor_chunks[8] is not None:
                            self.neighbors.append(neighbor_chunks[8][coord_y - height, coord_x - width] / 255)

                        # Edge case with no chunk for that neighbor, so adding 0 indicating black is added
                        else:
                            self.neighbors.append(0)

                    # Middle Left
                    elif 0 > coord_x and neighbor_chunks[3] is not None:
                        self.neighbors.append(neighbor_chunks[3][coord_y, coord_x] / 255)

                    # Middle Right
                    elif coord_x >= width and neighbor_chunks[5] is not None:
                        self.neighbors.append(neighbor_chunks[5][coord_y, coord_x - width] / 255)

                    # Edge case with no chunk for that neighbor, so adding 0 indicating black is added
                    else:
                        self.neighbors.append(0)

    def get_neighbors(self):
        """Returns the list of neighbors, with the pixel itself being the first item"""
        return self.neighbors


# MAIN
def main():
    """Main function for potential testing"""
    return 0


if __name__ == "__main__":
    sys.exit(main())
