"""
This python module contains many important and useful functions that did not feel at home in any of the
other classes. Thus these functions are kept in here for easy access when they are needed
"""


# IMPORTS
#   PYTHON MODULES
from itertools import product
import numpy as np
import sys

#   CUSTOM MODULES
from pixel_identification.pixel import Pixel


# META INFORMATION
__author__ = "Gijs Bakker & Jamie van Eijk"
__version = "1.0.0"


# FUNCTIONS
def get_key(dict, val):
    """
    By providing a dictionary and value, the key belonging to this value is returned
    :parameter dict: Target dictionary
    :parameter val: Value corresponding to the key
    :return: The extracted key
    """
    for key, value in dict.items():
        if val == value:
            return key


def pixel_objects_to_instances(pixels, color_dict):
    """
    Converts the Pixel objects to training instances and storing them as xs and ys
    :parameter pixels: List of pixel objects to be converted to instances
    :parameter color_dict: color dict containing the RGB codes (value) per label (key)
    :return: The xs and ys as numpy arrays
    """
    xs = []
    ys = []

    # For each pixel object
    for pixel in pixels:
        # Add the pixel and its neighbors as a list to xs
        xs.append(pixel.get_neighbors())

        # Append the label as an one hot encoding to ys
        label = get_key(color_dict, pixel.label)
        size = len(color_dict.keys())
        ys.append([0 if i != label else 1 for i in range(size)])

    return np.array(xs), np.array(ys)


def find_marked_locations(color_obj, train_obj, neighbor_range):
    """
    This function parses the color layer and training chunks to extract all the relevant pixels that can be used
    to train the model
    :parameter color_obj: Image Deconstructor for the color layer from which the shape,
                      chunks and downscaled chunks can be extracted
    :parameter train_obj: Image Deconstructor for the training image from which the shape,
                      chunks and downscaled chunks can be extracted
    :parameter neighbor_range: Amount of neighbors to look around and collect when creating the pixel objects
    :return: The color dictionary and a list of pixel objects for training
    """
    # Creating containers for the color dict and marked pixels list
    colors = {}
    marked_pixels = []

    # Getting the normal color layer and training image chunks
    color_chunks = color_obj.chunks
    train_chunks = train_obj.chunks

    # Creating a list with the total chunks including the normal ones and the downscaled ones from scale space
    total_chunks = [[1, train_chunks]]
    total_chunks += train_obj.downscaled

    # Iterate over every chunk in both the color and training chunks
    for chunk_y, (color_chunk_y, train_chunk_y) in enumerate(zip(color_chunks, train_chunks)):
        for chunk_x, (color_chunk_x, train_chunk_x) in enumerate(zip(color_chunk_y, train_chunk_y)):

            # Saving the current chunk coordinate
            current_chunk = (chunk_y, chunk_x)

            # Getting the 8 surrounding chunks
            neighbor_chunks = __get_neighboring_chunks(total_chunks, current_chunk, color_chunks)

            # Iterates over each instance
            for y_count, y in enumerate(color_chunk_x[0], start=0):
                for x_count, x in enumerate(y, start=0):

                    # Ignoring the pixel if it is marked as white
                    color = list(x)
                    if color != [255, 255, 255]:

                        # If a new color is found, add it to the dict as a new label
                        if color not in colors.values():
                            colors[len(colors.keys())] = color

                        # Turn the marked pixel into a Pixel object
                        pixel = Pixel((x_count, y_count), neighbor_range, label=color)

                        # Setting the neighboring pixels for each level of scaling
                        for scale in neighbor_chunks.keys():
                            pixel.set_close_pixels(neighbor_chunks[scale], scale)

                        # Append the Pixel object to the list with the others
                        marked_pixels.append(pixel)

    return colors, marked_pixels


def classify_image(test_chunks_obj, neighbor_range, md, color_dict, threshold):
    """
    Iterates over the test image chunks and turns each pixel into a Pixel object to get the neighbors
    These Pixel objects are then converted to instances and classified using the trained model.
    The classified results will then be parsed and a new numpy array structure will be constructed from scratch, which
    will eventually be used to render the classified image.
    To construct this array, each prediction will be compared to the threshold
    Is a prediction bigger then the threshold, append the color corresponding to that label to the array, otherwise
    it is not a certain prediction and the original color should be added to the array instead

    :parameter test_chunks_obj: Image Deconstructor for the test image from which the shape,
                            chunks and downscaled chunks can be extracted
    :parameter neighbor_range: Amount of neighbors to look around and collect when creating the pixel objects
    :parameter md: The trained/loaded model
    :parameter color_dict: color dict containing the RGB codes (value) per label (key)
    :parameter threshold: Threshold value indicating if a prediction is good or not
    :return: A numpy array containing the classified chunks
    """
    # Container for the classified chunks
    classified_chunks = []

    # Creating a list with the total chunks including the normal ones and the downscaled ones from scale space
    test_chunks = test_chunks_obj.chunks

    total_chunks = [[1, test_chunks]]
    total_chunks += test_chunks_obj.downscaled

    # Iterate over every chunk in the test chunks
    for chunk_y, test_chunk_y in enumerate(test_chunks):
        for chunk_x, test_chunk_x in enumerate(test_chunk_y):

            # Quick status update to show the user the program is still running
            print(f"Classifying chunk at y={chunk_y} x={chunk_x}")

            # More containers
            instances = []
            coords = []
            classified_chunk = []

            # Saving the current chunk coordinate
            current_chunk = (chunk_y, chunk_x)

            # Getting the 8 surrounding chunks
            neighbor_chunks = __get_neighboring_chunks(total_chunks, current_chunk, test_chunks)

            # Iterates over each instance
            for y_count, y in enumerate(test_chunk_x, start=0):
                for x_count, x in enumerate(y, start=0):

                    # Create pixel objects
                    pixel = Pixel((x_count, y_count), neighbor_range)

                    # Setting the neighboring pixels for each level of scaling
                    for scale in neighbor_chunks.keys():
                        pixel.set_close_pixels(neighbor_chunks[scale], scale)

                    # Creating instances to be classified
                    instance = pixel.get_neighbors()
                    if len(instance) != 0:
                        instances.append(instance)
                        coords.append((x_count, y_count))

            # Classify all the instances
            instances = np.array(instances)
            predicted_classes = md.classify(instances)

            # Creating dimensions to eventually refold the classified chunk list into the original chunks shape
            y_dim = (coords[-1][1] - coords[0][1]) + 1  # First y coordinate - Last y coordinate
            x_dim = (coords[-1][0] - coords[0][0]) + 1  # First x coordinate - Last x coordinate

            # Parsing the predictions
            for prediction, coord in zip(predicted_classes, coords):

                # If a prediction is 'significant', color the pixel
                max_value = max(prediction)
                if max_value >= threshold:
                    label = np.where(prediction == max_value)[0][0]
                    color = color_dict[label]
                    classified_chunk.extend(list(color))

                # Else give the original color
                else:
                    color = test_chunk_x[coord[1], coord[0]]
                    classified_chunk.extend([color, color, color])

            # Convert the list of color for this chunk into a Numpy array and fold it into the original structure
            classified_chunk = np.array(classified_chunk)
            classified_chunk = classified_chunk.reshape(y_dim, x_dim, 3)

            # Append the chunks to the list of total chunks
            classified_chunks.append(classified_chunk)

    return np.array(classified_chunks)


def __get_neighboring_chunks(total_chunks, current_chunk, chunks):
    """
    Collects all the surrounding chunks from the current chunk to get edge case pixels.
    If there is no neighboring chunk, None is used instead
    :parameter total_chunks: The array containing all the chunks (scale space and normal)
    :parameter current_chunk: The current chunk coordinate
    :parameter chunks: The normal chunks without scale space
    :return: A list of neighboring chunks for each scale space level
    """
    neighbor_chunks = {}  # Contains the neighboring chunks

    # Loops over all scale space levels
    for i in total_chunks:
        # Creating a dict like structure for the chunks
        neighbor_chunks[i[0]] = []

        # Setting the x and y ranges to get the 8 surrounding chunks
        y_neighbors = range(current_chunk[0] - 1, current_chunk[0] + 2)
        x_neighbors = range(current_chunk[1] - 1, current_chunk[1] + 2)

        # Iterates over the chunks within the 3 x 3 range
        for neighbor_chunk_y, neighbor_chunk_x in product(y_neighbors, x_neighbors):

            # If the neighboring chunk exists, add it
            if 0 <= neighbor_chunk_x < chunks.shape[1] and 0 <= neighbor_chunk_y < chunks.shape[0]:
                neighbor_chunks[i[0]].append(i[1][neighbor_chunk_y, neighbor_chunk_x])

            # Else add None because of an edge case
            else:
                neighbor_chunks[i[0]].append(None)
    return neighbor_chunks


def create_color_dict(image_array):
    """
    When a color dictionary needs to be created, but no chunking or other step is required, simply use this function
    :param image_array: An image array of a color layer to parse
    :return: A color dictionary
    """
    # Color dict container
    colors = {}

    # Iterating over each pixel
    for row in image_array:
        for color in row:

            # Ignoring the pixel if it is marked as white
            color = list(color)

            if color != [255, 255, 255]:

                # If a new color is found, add it to the dict as a label
                if color not in colors.values():
                    colors[len(colors.keys())] = color

    return colors


# MAIN
def main():
    """Main function for potential testing"""
    return 0


if __name__ == "__main__":
    sys.exit(main())
