"""
This module contains the model class

The model class will do the eventual classification of the EM-Image
When a good model already exists, it can be loaded and instantly used to classify,
otherwise a model can be build, trained and then used to classify
If this trained model performs well, it can be saved to any location
"""


# IMPORTS
from keras import models
from keras.optimizers import Adam
from keras.layers import Dense, InputLayer
import sys


# META INFORMATION
__author__ = "Gijs Bakker & Jamie van Eijk"
__version = "1.0.0"


# CLASSES
class Model:
    """
    The model class will do the eventual classification of the EM-Image
    When a good model already exists, it can be loaded and instantly used to classify,
    otherwise a model can be build, trained and then used to classify
    If this trained model performs well, it can be saved to any location
    """
    def __init__(self, dim, labels, learning_rate, model=None):
        """
        Setting the init parameters
        :parameter dim: The dimensions of the input instances
        :parameter labels: The amount of labels, used for inputs in the softmax layer
        :parameter learning_rate: The learning rate of the model when training
        :parameter model: A pre-saved model location which can be loaded, None if the model should be created
        """
        self.dim = dim
        self.labels = labels
        self.learning_rate = learning_rate

        # If no saved model is provided, build the model
        if model is None:
            self.model = self.__build()

        # Else load the given model
        else:
            self.model = models.load_model(model)
            print(f"Loaded model {model}")

    def __repr__(self):
        """Message when printing object"""
        return f"The model uses instances of {self.dim} dimensions as input to classify {self.labels} labels"

    def __build(self):
        """Creates the model and compiles it using Adam and Categorical Cross-Entropy"""
        # Creating the neural network
        model = models.Sequential()
        model.add(InputLayer(input_shape=(self.dim, )))
        model.add(Dense(32, activation='relu'))
        model.add(Dense(16, activation='relu'))
        model.add(Dense(self.labels, activation='softmax'))

        # Compiling the model
        opt = Adam(learning_rate=self.learning_rate)
        model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
        return model

    def fit(self, xs, ys, epochs, batch_size):
        """
        Trains the model for a requested amount of epochs on the training data
        :parameter xs: Training instances (list with pixel intensity and that of its neighbors)
        :parameter ys: Training classes (One hot encoded classes)
        :parameter epochs: The amount of epochs (iterations) the model should train
        :parameter batch_size: The sizes of the batches that the model should train on during the epochs
        """
        self.model.fit(xs, ys, epochs=epochs, batch_size=batch_size)

    def save_model(self, location):
        """Saves the current iteration of the model"""
        self.model.save(location)

    def classify(self, instances):
        """Classifies a set of instances using the current model"""
        return self.model.predict(instances)


# MAIN
def main():
    """Main function for potential testing"""
    return 0


if __name__ == "__main__":
    sys.exit(main())
